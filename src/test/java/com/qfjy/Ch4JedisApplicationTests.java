package com.qfjy;

import com.qfjy.entity.po.User;
import com.qfjy.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@SpringBootTest
class Ch4JedisApplicationTests {



    @Autowired
    private JedisPool jedisPool;

    @Test
    void contextLoads() {

        //连接池中获取连接Jedis  (JDBC Connection)
        Jedis jedis=jedisPool.getResource();

        jedis.set("javaName","JAVA2102班");

       String result= jedis.get("javaName");
        System.out.println("查询的Redis数据库的结果值是："+result);


        //关闭连接
        jedis.close();
    }

    @Autowired
    private UserServiceImpl userService;

    @Test
    void testString(){

        String key="javaInfo";
       String result=userService.getJavaInfo(key);
        System.out.println(result);
    }


    @Test
    void testHash(){

       User user= userService.selectUserById("1002");
        System.out.println(user);
    }

}
