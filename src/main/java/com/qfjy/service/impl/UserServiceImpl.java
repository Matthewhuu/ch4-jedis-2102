package com.qfjy.service.impl;

import com.qfjy.entity.po.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName UserServiceImpl
 * @Description TODO  Redis的目的就是为了解决MYSQL对高性能查询修改等不足。
 *  Redis就是用来解决频繁的请求等问题。 性能非常好
 * @Author guoweixin
 * @Date 2021/8/5
 * @Version 1.0
 */
@Slf4j
@Service
public class UserServiceImpl {


    @Autowired
     private JedisPool jedisPool;

    /**
     * 模拟Redis String类型相关命令存取操作
     * Jedis 和Redis的命令是相辅相成的。
     *  Redis有哪些命令，Jedis就有哪些方法
     */
    /**
     * 模拟String存和取过程
     * 判断key在Redis中是否存在
     * 如果不存在，查询MYSQL，将MYSQL的查询结果， 存入Redis key中，进行返回
     * 如果存在，直接查询Redis
     */
    public String getJavaInfo(String key){
        //获取Jedis连接
        Jedis jedis=jedisPool.getResource();
        String result=null;

        if(jedis.exists(key)){
            log.info("查询的是Redis数据库：");

          // jedis.expire()
           result=jedis.get(key);

        }else{
            log.info("查询的是MYSQL的数据，并给予结果返回");
            result="千锋南京2102班";
            jedis.set(key,result);
        }

        //关闭Jedis连接
        jedis.close();
        return result;
    }

    /**
     * Redis数据结构（类型）Hash
     * Hash特别适合存储一个对象
     *
     * 判断Redis中的KEY是否存在，
     * 如果不存在，查询MYSQL，将结果得到存入Redis Hash类型
     * 如果存在，直接查询Redis 并返回
     * key命令建议  user:id
     */


    public User selectUserById(String id){
        String key="user:"+id;
        User user=null;
        Jedis jedis=jedisPool.getResource();
        if(jedis.exists(key)){
            log.info("查询的是Redis数据库");

           Map<String,String> map= jedis.hgetAll(key);
            user=new User();
            user.setId(map.get("id"));
            user.setName(map.get("name"));
            user.setAge(Integer.parseInt(map.get("age")));


        }else{
            log.info("查询的是MYSQL数据库--》");
             user=new User();
            user.setId(id);
            user.setName("张三");
            user.setAge(20);

            //将User对象存入Redis  hash
            Map<String,String> map=new HashMap<>();
            map.put("id",user.getId());
            map.put("name",user.getName());
            map.put("age",user.getAge()+"");
            jedis.hset(key,map);

        }


        // userMapper.selectUserById(id);  查询MYSQL

        jedis.close();
        return user;
    }



}
