package com.qfjy.entity.po;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName User
 * @Description TODO
 * @Author guoweixin
 * @Date 2021/8/5
 * @Version 1.0
 */
@Data
public class User implements Serializable {

    private String id;
    private String name;
    private int age;
}
