package com.qfjy.config;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @ClassName JedisConfig
 * @Description TODO
 * @Author guoweixin
 * @Date 2021/8/5
 * @Version 1.0
 */
@Configuration
@ConfigurationProperties(prefix = "spring.redis")
public class JedisConfig {


    private int database;
    private int port;
    private String host;
    private String password;
    private int timeout;



    @Value("${spring.redis.jedis.pool.max-idle}")
    private int maxTotal;
    @Value("${spring.redis.jedis.pool.min-idle}")
    private int minIdel;

    /**
     * 连接池   JDBC连接池
     * Jedis --->连接池     Connection -->JDBC连接池
     */

    @Bean
    public JedisPool jedisPool(){

        //连接池基本配置  初始连接数，最大连接，最大空闲数....
        JedisPoolConfig jedisPoolConfig=new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(10);
        jedisPoolConfig.setMaxIdle(10);
        jedisPoolConfig.setMinIdle(1);

        //连接池 创建  连接池配置，IP地址,port,   timeout, password
        JedisPool jedisPool=new JedisPool(jedisPoolConfig,host,port,timeout,password);

        return jedisPool;
    }










    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }
    public int getDatabase() {
        return database;
    }

    public void setDatabase(int database) {
        this.database = database;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
